<?php

namespace App\Controller;

use App\Entity\Area;
use App\Repository\AreaRepository;
use App\Service\MessageGenerator;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/area')]
class AreaController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Area::class;
    }

    public function __construct(private readonly MessageGenerator $messageGenerator) {}

    #[Route('', name: "createArea", methods: ['POST'])]
    public function createArea(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $area = $serializer->deserialize($request->getContent(), Area::class, 'json');
        $em->persist($area);
        $em->flush();

        $jsonArea = $serializer->serialize($area, 'json', []);

        //$location = $urlGenerator->generate('detailArea', ['id' => $area->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        $message = $this->messageGenerator->getHappyMessage($area->getName());

        return new JsonResponse($jsonArea, Response::HTTP_CREATED, [/*"Location" => $location*/], true);
    }

    #[Route('', name: "getAllAreas", methods: ['GET'])]
    public function getAllAreas(AreaRepository $areaRepository, SerializerInterface $serializer, LoggerInterface $logger): JsonResponse
    {
        $logger->info('Look, I just used a service!');

        $message = $this->messageGenerator->getHappyMessage("GET ALL AREAS");

        $areaList = $areaRepository->findAll();

        $jsonAreaList = $serializer->serialize($areaList, 'json', [/*'groups' => 'getAreas'*/]);
        return new JsonResponse($message, Response::HTTP_OK, [], true);
    }
}
