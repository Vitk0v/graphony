<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class MyFirstListener
{
    public function onKernelRequest(RequestEvent $event) {
        echo "MY FIRST LISTENER ";
    }

    public function onKernelRequestBlocker(RequestEvent $event) {
        // Your code here
    }

    public function onKernelException(ExceptionEvent $event) {
        // Your code here
    }
}