<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class MySecondListener
{
    #[AsEventListener]
    public function onKernelRequest(RequestEvent $event) {
        echo "MY SECOND LISTENER ,";
    }

    #[AsEventListener(event: 'kernel.request')]
    public function custom(RequestEvent $event) {
        echo "CUSTOM ,";
    }

}