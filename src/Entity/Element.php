<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\ElementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ElementRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new Post(),
        new Delete(),
        new Patch(),
    ]
)]
class Element
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, Monster>
     */
    #[ORM\ManyToMany(targetEntity: Monster::class, mappedBy: 'weaknesses')]
    private Collection $weaknessOf;

    /**
     * @var Collection<int, Weapon>
     */
    #[ORM\ManyToMany(targetEntity: Weapon::class, mappedBy: 'element')]
    private Collection $weapons;

    public function __construct()
    {
        $this->weaknessOf = new ArrayCollection();
        $this->weapons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Monster>
     */
    public function getWeaknessOf(): Collection
    {
        return $this->weaknessOf;
    }

    public function addWeaknessOf(Monster $weaknessOf): static
    {
        if (!$this->weaknessOf->contains($weaknessOf)) {
            $this->weaknessOf->add($weaknessOf);
            $weaknessOf->addWeakness($this);
        }

        return $this;
    }

    public function removeWeaknessOf(Monster $weaknessOf): static
    {
        if ($this->weaknessOf->removeElement($weaknessOf)) {
            $weaknessOf->removeWeakness($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Weapon>
     */
    public function getWeapons(): Collection
    {
        return $this->weapons;
    }

    public function addWeapon(Weapon $weapon): static
    {
        if (!$this->weapons->contains($weapon)) {
            $this->weapons->add($weapon);
            $weapon->addElement($this);
        }

        return $this;
    }

    public function removeWeapon(Weapon $weapon): static
    {
        if ($this->weapons->removeElement($weapon)) {
            $weapon->removeElement($this);
        }

        return $this;
    }
}
