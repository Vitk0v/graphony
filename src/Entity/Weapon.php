<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\WeaponRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: WeaponRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new Post(),
        new Delete(),
        new Patch(),
    ],
    normalizationContext: ['groups' => ['read:Weapon:collection']]
)]
class Weapon
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Weapon:collection'])]
    private ?string $name = null;

    /**
     * @var Collection<int, Element>
     */
    #[ORM\ManyToMany(targetEntity: Element::class, inversedBy: 'weapons')]
    private Collection $element;

    public function __construct()
    {
        $this->element = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Element>
     */
    public function getElement(): Collection
    {
        return $this->element;
    }

    public function addElement(Element $element): static
    {
        if (!$this->element->contains($element)) {
            $this->element->add($element);
        }

        return $this;
    }

    public function removeElement(Element $element): static
    {
        $this->element->removeElement($element);

        return $this;
    }
}
