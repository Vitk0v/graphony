<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240530112139 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE weapon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weapon_element (weapon_id INT NOT NULL, element_id INT NOT NULL, INDEX IDX_E4771E7D95B82273 (weapon_id), INDEX IDX_E4771E7D1F1F2A24 (element_id), PRIMARY KEY(weapon_id, element_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE weapon_element ADD CONSTRAINT FK_E4771E7D95B82273 FOREIGN KEY (weapon_id) REFERENCES weapon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE weapon_element ADD CONSTRAINT FK_E4771E7D1F1F2A24 FOREIGN KEY (element_id) REFERENCES element (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weapon_element DROP FOREIGN KEY FK_E4771E7D95B82273');
        $this->addSql('ALTER TABLE weapon_element DROP FOREIGN KEY FK_E4771E7D1F1F2A24');
        $this->addSql('DROP TABLE weapon');
        $this->addSql('DROP TABLE weapon_element');
    }
}
