<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240530103307 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE monster_element (monster_id INT NOT NULL, element_id INT NOT NULL, INDEX IDX_E321EF06C5FF1223 (monster_id), INDEX IDX_E321EF061F1F2A24 (element_id), PRIMARY KEY(monster_id, element_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE monster_element ADD CONSTRAINT FK_E321EF06C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE monster_element ADD CONSTRAINT FK_E321EF061F1F2A24 FOREIGN KEY (element_id) REFERENCES element (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE monster_element DROP FOREIGN KEY FK_E321EF06C5FF1223');
        $this->addSql('ALTER TABLE monster_element DROP FOREIGN KEY FK_E321EF061F1F2A24');
        $this->addSql('DROP TABLE monster_element');
    }
}
